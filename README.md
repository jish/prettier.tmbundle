## Prettier TextMate 2 bundle

* TextMate https://macromates.com/
* Prettier https://prettier.io

### Installation

    mkdir -p ~/Library/Application\ Support/TextMate/Pristine\ Copy/Bundles
    cd ~/Library/Application\ Support/TextMate/Pristine\ Copy/Bundles
    git clone git@gitlab.com:jish/prettier.tmbundle.git Prettier.tmbundle

### Usage

As far as I know all TextMate bundles bind `ctrl` + `shift` + `H` to
"Reformat Document", so this bundle adheres to that standard.

Press `⌃⇧H` to "Reformat Document" with Prettier.
